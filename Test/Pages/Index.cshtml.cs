﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Test.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly HttpClient client = null;

        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public async Task<IActionResult> OnPost(LoginModel model)
        {
            var postTask = client.PostAsJsonAsync<LoginModel>("https://localhost:7224/api/Member/Login", model);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return Redirect("/Members");
            }
            return Page();
        }   
    }
}