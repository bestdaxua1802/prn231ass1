﻿using DataAccess.Commons;


namespace eStoreAPIs
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services, AppConfiguration appConfiguration)
        {
            services.AddHttpContextAccessor();
            return services;
        }
    }
}
