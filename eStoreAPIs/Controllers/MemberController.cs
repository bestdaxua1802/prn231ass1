﻿using Azure;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.MemberModels;
using DataAccess.Models.OrderModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace eStoreAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IMemberRepository _memberRepository;

        public MemberController(IMemberRepository memberRepository)
        {
            _memberRepository = memberRepository;
        }

        [HttpGet("GetAllMember")]
        public async Task<Pagination<Member>> GetAllMember(int pageIndex, int pageSize) => await _memberRepository.GetAllMemberAsync(pageIndex = 0, pageSize = 10);


        [HttpPut("UpdateMember/{memberId}")]
        public async Task<IActionResult> UpdateMember(int memberId, ViewMemberModel member) {
            var memberObj = await _memberRepository.UpdateMemberAsync(memberId, member);
            if (memberObj != null) {
                return Ok("Update Success");
            }
            return BadRequest("MemberId not found or Invalid input ");
        }

        [HttpGet("GetMemberByMemberId/{MemberId}")]
        public async Task<IActionResult> GetMemberByMemberId(int MemberId) {
            var memberObj = await _memberRepository.GetMemberById(MemberId);
            if (memberObj == null) {
                return BadRequest("MemberId not found");
            }
            return Ok(memberObj);
        }
        [HttpDelete("DeleteMember/{memberId}")]
        public async Task<IActionResult> DeleteMember(int memberId)
        {
            var memberObj = await _memberRepository.DeleteMember(memberId);
            if (memberObj == true)
            {
                return Ok("Delete Success");
            }
            return BadRequest("MemberId not found");
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var memberObj = await _memberRepository.Login(model);
            if (memberObj != null)
            {
                return Ok(memberObj);
            }
            return BadRequest("Login Failed");
        }

        [HttpPost("CreateMember")]
        public async Task<IActionResult> CreateMember(GetAllMemberModel member)
        {
            var memberObj = await _memberRepository.CreateMemberAsync(member);
            if (memberObj != null)
            {
                return Ok("Create Success");
            }
            return BadRequest("Invalid input");
        }
    }
}
