﻿using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.CategoryModels;
using DataAccess.Models.MemberModels;
using DataAccess.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet("GetAllCategory")]
        public async Task<List<ViewCategoryModel>> GetAllCategory() => await _categoryRepository.GetAllCAtegoryAsync();
    }
}
