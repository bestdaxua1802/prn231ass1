﻿using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.OrderModels;
using DataAccess.Models.ProductModels;
using DataAccess.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        [HttpGet("GetAllOrders")]
        public async Task<Pagination<ViewOrderModel>> GetAllOrders(int pageIndex, int pageSize) => await _orderRepository.GetAllOrderAsync(pageIndex = 0, pageSize = 10);


        [HttpPut("UpdateOrder/{OrderId}")]
        public async Task<IActionResult> UpdateOrder(int OrderId, UpdateOrderModel order)
        {
            var orderObj = await _orderRepository.UpdateOrderAsync(OrderId, order);
            if (orderObj != null)
            {
                return Ok("Update Success");
            }
            return BadRequest("ProductId not found or Invalid input ");
        }

        [HttpGet("GetOrderByOrderId/{OrderId}")]
        public async Task<IActionResult> GetOrderByOrderId(int OrderId)
        {
            var orderObj = await _orderRepository.GetOrderById(OrderId);
            if (orderObj == null)
            {
                return BadRequest("OrderId not found");
            }
            return Ok(orderObj);
        }

        [HttpGet("GetOrderByMemberId/{memberId}")]
        public async Task<IActionResult> GetOrderByMemberId(int memberId, int pageNumber = 0, int pageSize = 10)
        {
            var productObj = await _orderRepository.GetOrderByMemberId(memberId, pageNumber, pageSize);
            if (productObj == null)
            {
                return BadRequest("No Order Have The Same MemberId Found");
            }
            return Ok(productObj);
        }

        [HttpDelete("DeleteOrder/{OrderId}")]
        public async Task<IActionResult> DeleteOrder(int OrderId)
        {
            var productObj = await _orderRepository.DeleteOrder(OrderId);
            if (productObj == true)
            {
                return Ok("Delete Success");
            }
            return BadRequest("OrderId not found");
        }

        [HttpPost("CreateOrder")]
        public async Task<IActionResult> CreateOrder(CreateOrderModel order)
        {
            var orderObj = await _orderRepository.CreateOrderAsync(order);
            if (orderObj != null)
            {
                return Ok("Create Success");
            }
            return BadRequest("Invalid input");
        }
    }
}
