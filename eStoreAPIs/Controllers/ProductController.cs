﻿using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.MemberModels;
using DataAccess.Models.ProductModels;
using DataAccess.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eStoreAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet("GetAllProducts")]
        public async Task<List<Product>> GetAllProducts() => await _productRepository.GetAllProductAsync();


        [HttpPut("UpdateProduct/{productId}")]
        public async Task<IActionResult> UpdateProduct(int productId, UpdateProductModel product)
        {
            var productObj = await _productRepository.UpdateProductAsync(productId, product);
            if (productObj != null)
            {
                return Ok("Update Success");
            }
            return BadRequest("ProductId not found or Invalid input ");
        }

        [HttpGet("GetProductByProductId/{productId}")]
        public async Task<IActionResult> GetProductByProductId(int productId)
        {
            var productObj = await _productRepository.GetProductById(productId);
            if (productObj == null)
            {
                return BadRequest("ProductId not found");
            }
            return Ok(productObj);
        }

        [HttpGet("GetProductByUnitPrice/{unitprice}")]
        public async Task<IActionResult> GetProductByUnitPrice(decimal UnitPrice)
        {
            var productObj = await _productRepository.GetProductByUnitPrice(UnitPrice);
            if (productObj == null)
            {
                return BadRequest("No Product Have The Same UnitPrice Found");
            }
            return Ok(productObj);
        }

        [HttpGet("GetProductByName/{productName}")]
        public async Task<IActionResult> GetProductByName(string productName)
        {
            var productObj = await _productRepository.GetProductByName(productName);
            if (productObj == null)
            {
                return BadRequest("No Product Have The Same Name Found");
            }
            return Ok(productObj);
        }


        [HttpDelete("DeleteProduct/{productId}")]
        public async Task<IActionResult> DeleteProduct(int productId)
        {
            var productObj = await _productRepository.DeleteProduct(productId);
            if (productObj == true)
            {
                return Ok("Delete Success");
            }
            return BadRequest("ProductId not found");
        }

        [HttpPost("CreateProduct")]
        public async Task<IActionResult> CreateProduct(ViewProductModel product)
        {
            var productObj = await _productRepository.CreateProductAsync(product);
            if (productObj != null)
            {
                return Ok("Create Success");
            }
            return BadRequest("Invalid input");
        }
    }
}
