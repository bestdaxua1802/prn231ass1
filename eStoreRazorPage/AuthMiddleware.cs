﻿public class AuthMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (!context.Request.Path.Value.ToLower().Contains("movie"))
        {
            await next(context);
            return;
        }
        int? role = context.Session.GetInt32("role");

        if (role == null && !context.Request.Path.Value.ToLower().Contains("login"))
        {
            context.Response.Redirect("/Login");
            return;
        }
        else
        if (role != 1 && !context.Request.Path.Value.ToLower().Contains("error"))
        {
            context.Response.Redirect("/Error");
            return;
        }

        await next(context);
    }
}