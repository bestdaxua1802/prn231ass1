﻿using DataAccess.Models.OrderDetailModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.OrderModels
{
    public class CreateOrderModel
    {
        public CreateOrderModel()
        {
            OrderDetails = new HashSet<ContactOrderDetailModel>();
        }

        public int OrderId { get; set; }
        public int? MemberId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public decimal? Freight { get; set; }
        public virtual ICollection<ContactOrderDetailModel> OrderDetails { get; set; }
    }
}
