﻿using BusinessObject.Models;
using DataAccess.Models.OrderDetailModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.OrderModels
{
    public class UpdateOrderModel
    {
        public UpdateOrderModel()
        {
            OrderDetails = new HashSet<ContactOrderDetailModel>();
        }
        public DateTime OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public decimal? Freight { get; set; }
        public virtual ICollection<ContactOrderDetailModel> OrderDetails { get; set; }
    }
}
