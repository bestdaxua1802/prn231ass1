﻿using BusinessObject.Models;
using DataAccess.Models.OrderDetailModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.OrderModels
{
    public class ViewOrderModel
    {
        public ViewOrderModel()
        {
            OrderDetails = new HashSet<ViewOrderDetailModel>();
        }

        public int OrderId { get; set; }
        public int? MemberId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public decimal? Freight { get; set; }
        public virtual ICollection<ViewOrderDetailModel> OrderDetails { get; set; }
    }
}
