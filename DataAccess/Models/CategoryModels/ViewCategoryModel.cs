﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models.CategoryModels
{
    public class ViewCategoryModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; } = null!;
    }
}
