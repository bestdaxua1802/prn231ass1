using AutoMapper;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Models.CategoryModels;
using DataAccess.Models.MemberModels;
using DataAccess.Models.OrderDetailModel;
using DataAccess.Models.OrderModels;
using DataAccess.Models.ProductModels;

namespace DataAccess
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Member, ViewMemberModel>().ReverseMap();
            CreateMap<Member, GetAllMemberModel>().ReverseMap();
            CreateMap<Product, UpdateProductModel>().ReverseMap();
            CreateMap<Product, ViewProductModel>().ReverseMap();
            CreateMap<Category, ViewCategoryModel>().ReverseMap();
            CreateMap<Order, ViewOrderModel>().ReverseMap();
            CreateMap<Order, UpdateOrderModel>().ReverseMap();
            CreateMap<Order, CreateOrderModel>().ReverseMap();
            CreateMap<OrderDetail, ViewOrderDetailModel>().ReverseMap();
            CreateMap<OrderDetail, ContactOrderDetailModel>().ReverseMap();
            /* pagination */
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

        }
    }
}
