﻿using AutoMapper;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DataAccess.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected DbSet<TEntity> _dbSet;
        private readonly IMapper _mapper;

        public GenericRepository(AppDBContext appDBContext, IMapper mapper ) // contructor 3 param
        {
            _dbSet = appDBContext.Set<TEntity>();
            _mapper = mapper;
        }

        public async Task AddAsync(TEntity entity)
        {

            await _dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public Task<List<TEntity>> GetAllAsync() => _dbSet.ToListAsync();


        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            _dbSet.UpdateRange(entities);
        }
        public async Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> expression)
        {
            var data = await _dbSet.Where(expression).ToListAsync();
            return data;
        }
        public IQueryable<TEntity> query()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<Pagination<TEntity>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<TEntity>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }
    }
}
