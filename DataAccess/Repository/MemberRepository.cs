﻿using AutoMapper;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.MemberModels;
using DataAccess.Models.OrderModels;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Web.Models;

namespace DataAccess.Repository
{
    public class MemberRepository : GenericRepository<Member>, IMemberRepository
    {
        private readonly AppDBContext _dbContext;
        private readonly IMapper _mapper;
        public MemberRepository(AppDBContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<Member?> Login(LoginModel model)
        {
            var member = await _dbContext.Members.FirstOrDefaultAsync(x => x.Email == model.Email && x.Password == model.Password);
            if (member == null)
            {
                return null;
            }

            return member;
        }

        public async Task<ViewMemberModel> UpdateMemberAsync(int memberId, ViewMemberModel memberDTO)
        {
            var memberObj = await _dbContext.Members.FirstOrDefaultAsync(x => x.MemberId == memberId);
            if (memberObj != null)
            {
                _mapper.Map(memberDTO, memberObj);
                _dbSet.Update(memberObj);
                var isSuccess = await _dbContext.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<ViewMemberModel>(memberObj);
                }
                return null;
            }
            return null;
        }

        public async Task<Pagination<Member>> GetAllMemberAsync(int pageIndex, int pageSize)
        {
			var itemCount = await _dbContext.Members.CountAsync();
			var items = await _dbContext.Members
									.Skip(pageIndex * pageSize)
									.Take(pageSize)
									.AsNoTracking()
									.ToListAsync();

			var result = new Pagination<Member>()
			{
				PageIndex = pageIndex,
				PageSize = pageSize,
				TotalItemsCount = itemCount,
				Items = _mapper.Map<List<Member>>(items),
			};

			return result;
		}

        public async Task<ViewMemberModel> GetMemberById(int memberId)
        {
            var result = await _dbContext.Members.FirstOrDefaultAsync(x => x.MemberId == memberId);
            if (result == null)
            {
                return null;
            }
            return _mapper.Map<ViewMemberModel>(result);
        }

        public async Task<Boolean> DeleteMember(int memberId)
        {
            var result = await _dbContext.Members.FirstOrDefaultAsync(x => x.MemberId == memberId);
            if (result == null)
            {
                return false;
            }
            else
            {
                _dbSet.Remove(result);
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }

        public async Task<GetAllMemberModel> CreateMemberAsync(GetAllMemberModel memberDTO)
        {
            var result = await _dbContext.Members.FirstOrDefaultAsync(x => x.MemberId == memberDTO.MemberId);
            if (result != null)
            {
                return null;
            }
            var memberobj = _mapper.Map<Member>(memberDTO);
            _dbSet.AddAsync(memberobj);
            var isSuccess = await _dbContext.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<GetAllMemberModel>(memberobj);
            }
            return null;
        }
    }
}