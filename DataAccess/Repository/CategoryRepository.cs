﻿using AutoMapper;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.CategoryModels;
using DataAccess.Models.MemberModels;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly AppDBContext _dbContext;
        private readonly IMapper _mapper;
        public CategoryRepository(AppDBContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<ViewCategoryModel>> GetAllCAtegoryAsync()
        {
            var result = _dbContext.Categories.ToList();
            return _mapper.Map<List<ViewCategoryModel>>(result);
        }
    }
}
