﻿using AutoMapper;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.MemberModels;
using DataAccess.Models.ProductModels;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private readonly AppDBContext _dbContext;
        private readonly IMapper _mapper;
        public ProductRepository(AppDBContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<Product>> GetProductByName(string ProductName)
        {
            var itemCount = await _dbContext.Products.Where(x => x.ProductName.Contains(ProductName)).CountAsync();
            var items = await _dbContext.Products.Where(x => x.ProductName.Contains(ProductName))
                                    .AsNoTracking()
                                    .ToListAsync();
            if (itemCount == 0)
            {
                return null;
            }

            return items;
        }

        public async Task<List<Product>> GetProductByUnitPrice(decimal UnitPrice)
        {
            var itemCount = await _dbContext.Products.Where(x => x.UnitPrice == UnitPrice).CountAsync();
            var items = await _dbContext.Products.Where(x => x.UnitPrice == UnitPrice)
                                    .AsNoTracking()
                                    .ToListAsync();
            if (itemCount == 0)
            {
                return null;
            }
            return items;
        }

        public async Task<UpdateProductModel> UpdateProductAsync(int productId, UpdateProductModel productDTO)
        {
            var producObj = await _dbContext.Products.FirstOrDefaultAsync(x => x.ProductId == productId);
            if (producObj != null)
            {
                _mapper.Map(productDTO, producObj);
                _dbSet.Update(producObj);
                var isSuccess = await _dbContext.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateProductModel>(producObj);
                }
                return null;
            }
            return null;
        }

        public async Task<List<Product>>GetAllProductAsync()
        {
            var items = await _dbContext.Products
                                    .AsNoTracking()
                                    .ToListAsync();
            return items;
        }

        public async Task<ViewProductModel> GetProductById(int productId)
        {
            var result = await _dbContext.Products.FirstOrDefaultAsync(x => x.ProductId == productId);
            if (result == null)
            {
                return null;
            }
            return _mapper.Map<ViewProductModel>(result);
        }

        public async Task<Boolean> DeleteProduct(int productId)
        {
            var result = await _dbContext.Products.FirstOrDefaultAsync(x => x.ProductId == productId);
            if (result == null)
            {
                return false;
            }
            else
            {
                _dbSet.Remove(result);
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }

        public async Task<ViewProductModel> CreateProductAsync(ViewProductModel productDTO)
        {
            var result = await _dbContext.Products.FirstOrDefaultAsync(x => x.ProductId == productDTO.ProductId);
            if (result != null)
            {
                return null;
            }
            var productDObj = _mapper.Map<Product>(productDTO);
            _dbSet.AddAsync(productDObj);
            var isSuccess = await _dbContext.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<ViewProductModel>(productDObj);
            }
            return null;
        }
    }
}
