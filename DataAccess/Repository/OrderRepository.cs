﻿using AutoMapper;
using AutoMapper.Execution;
using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Interface;
using DataAccess.Models.MemberModels;
using DataAccess.Models.OrderModels;
using DataAccess.Models.ProductModels;
using DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private readonly AppDBContext _dbContext;
        private readonly IMapper _mapper;
        public OrderRepository(AppDBContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<CreateOrderModel> CreateOrderAsync(CreateOrderModel orderDTO)
        {
            var result = await _dbContext.Orders.FirstOrDefaultAsync(x => x.OrderId == orderDTO.OrderId);
            if (result != null)
            {
                return null;
            }
            var orderObj = _mapper.Map<Order>(orderDTO);
            var checkMember = await _dbContext.Members.FirstOrDefaultAsync(x => x.MemberId == orderDTO.MemberId);
            foreach (var item in orderDTO.OrderDetails) {
                var checkProduct = await _dbContext.Products.FirstOrDefaultAsync(x => x.ProductId == item.ProductId);
                if (checkProduct == null)
                {
                    return null;
                }
            }
            if (checkMember == null)
            {
                return null;
            }
            foreach (var item in orderObj.OrderDetails)
            {
                item.OrderId = orderDTO.OrderId;
            }
                _dbSet.AddAsync(orderObj);
            var isSuccess = await _dbContext.SaveChangesAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<CreateOrderModel>(orderObj);
            }
            return null;
        }

        public async Task<bool> DeleteOrder(int orderId)
        {
            var result = await _dbContext.Orders.FirstOrDefaultAsync(x => x.OrderId == orderId);
            if (result == null)
            {
                return false;
            }
            else
            {
                _dbSet.Remove(result);
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }

        public async Task<Pagination<ViewOrderModel>> GetAllOrderAsync(int pageIndex, int pageSize)
        {
            var itemCount = await _dbContext.Orders.CountAsync();
            var items = await _dbContext.Orders.Include(x => x.OrderDetails)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<ViewOrderModel>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = _mapper.Map<List<ViewOrderModel>>(items),
            };

            return result;
        }

        public async Task<ViewOrderModel> GetOrderById(int orderId)
        {
            var result = await _dbContext.Orders.Include(x => x.OrderDetails).FirstOrDefaultAsync(x => x.OrderId == orderId);
            if (result == null)
            {
                return null;
            }
            return _mapper.Map<ViewOrderModel>(result);
        }

        public async Task<Pagination<ViewOrderModel>> GetOrderByMemberId(int memberId, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.Orders.Include(x => x.OrderDetails).Where(x => x.MemberId == memberId).CountAsync();
            var items = await _dbContext.Orders.Where(x => x.MemberId == memberId)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();
            if (itemCount == 0)
            {
                return null;
            }

            var result = new Pagination<ViewOrderModel>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = _mapper.Map<List<ViewOrderModel>>(items),
            };

            return result;
        }

        public async Task<UpdateOrderModel> UpdateOrderAsync(int orderId, UpdateOrderModel orderDTO)
        {
            var orderObj = await _dbContext.Orders.FirstOrDefaultAsync(x => x.OrderId == orderId);
            if (orderObj != null)
            {
                _mapper.Map(orderDTO, orderObj);
                _dbSet.Update(orderObj);
                var isSuccess = await _dbContext.SaveChangesAsync() > 0;
                if (isSuccess)
                {
                    return _mapper.Map<UpdateOrderModel>(orderObj);
                }
                return null;
            }
            return null;
        }
    }
}
