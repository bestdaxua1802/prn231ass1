﻿using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Models.CategoryModels;
using DataAccess.Models.MemberModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        public Task<List<ViewCategoryModel>> GetAllCAtegoryAsync();
    }
}
