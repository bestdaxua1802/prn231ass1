﻿using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Models.MemberModels;
using DataAccess.Models.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Models;

namespace DataAccess.Interface
{
    public interface IMemberRepository : IGenericRepository<Member>
    {
        public Task<Member?> Login(LoginModel model);
        public Task<ViewMemberModel> UpdateMemberAsync(int memberId, ViewMemberModel memberDTO);
        public Task<GetAllMemberModel> CreateMemberAsync(GetAllMemberModel memberDTO);
        public Task<Pagination<Member>> GetAllMemberAsync(int pageIndex, int pageSize);
        public Task<ViewMemberModel> GetMemberById(int memberId);
        public Task<Boolean> DeleteMember(int memberId);
    }
}
