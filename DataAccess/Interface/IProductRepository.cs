﻿using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Models.MemberModels;
using DataAccess.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        public Task<List<Product>> GetProductByName(string QuizzName);
        public Task<List<Product>> GetProductByUnitPrice(decimal UnitPrice);
        public Task<UpdateProductModel> UpdateProductAsync(int productId, UpdateProductModel productDTO);
        public Task<ViewProductModel> CreateProductAsync(ViewProductModel memberDTO);
        public Task<List<Product>> GetAllProductAsync();
        public Task<ViewProductModel> GetProductById(int productId);
        public Task<Boolean> DeleteProduct(int productId);
    }
}
