﻿using DataAccess.Commons;
using BusinessObject.Models;
using System.Linq.Expressions;

namespace DataAccess.Interface
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetAllAsync();
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        void UpdateRange(List<TEntity> entities);
        Task AddRangeAsync(List<TEntity> entities);
        Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> query();
        Task<Pagination<TEntity>> ToPagination(int pageNumber = 0, int pageSize = 10);
    }
}
