﻿using BusinessObject.Models;
using DataAccess.Commons;
using DataAccess.Models.OrderDetailModel;
using DataAccess.Models.OrderModels;
using DataAccess.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        public Task<UpdateOrderModel> UpdateOrderAsync(int orderId, UpdateOrderModel orderDTO);
        public Task<CreateOrderModel> CreateOrderAsync(CreateOrderModel orderDTO);
        public Task<Pagination<ViewOrderModel>> GetAllOrderAsync(int pageIndex, int pageSize);
        public Task<ViewOrderModel> GetOrderById(int orderId);
        public Task<Boolean> DeleteOrder(int orderId);
        Task<Pagination<ViewOrderModel>> GetOrderByMemberId(int memberId, int pageNumber = 0, int pageSize = 10);
    }
}
