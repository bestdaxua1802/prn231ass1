﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObject.Models;
using System.Net.Http.Headers;
using System.Text.Json;
using Web.Models;
using Newtonsoft.Json;
using System.Text;
using System.Reflection.Metadata;

namespace Web.Controllers
{
    public class MembersController : Controller
    {
        private readonly HttpClient _client ;

        Uri baseAddress = new Uri("https://localhost:7224/api");

        public MembersController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;

        }

		[HttpGet]
		public IActionResult Product()
		{
			return RedirectToAction("Index", "Product");
		}

		[HttpGet]
		public IActionResult Order()
		{
			return RedirectToAction("Index", "Order");
		}

		// GET: Members
		[HttpGet]
        public IActionResult Index()
        {
            List<Member> memberList = new List<Member>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Member/GetAllMember").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                memberList = JsonConvert.DeserializeObject<List<Member>>(data);
            }
            return View(memberList);
        }


        // GET: Members/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(MemberViewModel member)
        {
            try
            {
                string data = JsonConvert.SerializeObject(member);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/Member/CreateMember", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["Success"] = "Member Created";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Member Create Failed";
                return View();
            }
            return View();
        }

        // GET: Members/Update
        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {

                Member member = new Member();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Member/GetMemberByMemberId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    member = JsonConvert.DeserializeObject<Member>(data);
                    member.MemberId = id;
                }
                return View(member);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost]
        public IActionResult Edit(Member member)
        {
            try
            {
                string data = JsonConvert.SerializeObject(member);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PutAsync(_client.BaseAddress + "/Member/UpdateMember/" + member.MemberId, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Member Update Failed";
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            try
            {
                Member member = new Member();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Member/GetMemberByMemberId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    member = JsonConvert.DeserializeObject<Member>(data);
                    member.MemberId = id;
                }
                return View(member);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + "/Member/DeleteMember/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["Success"] = "Member Deleted";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Member Update Failed";
                return View();
            }
            return View();
        }
    }
}
