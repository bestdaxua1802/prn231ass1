﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json;
using System.Text;
using Web.Models;

namespace Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly HttpClient _client;

        Uri baseAddress = new Uri("https://localhost:7224/api");

        public ProductController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;

        }

        [HttpGet]
        public IActionResult Index()
        {
            List<Product> productList = new List<Product>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Product/GetAllProducts").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                productList = JsonConvert.DeserializeObject<List<Product>>(data);
            }
            return View(productList);
        }


		[HttpGet]
		public IActionResult Order()
		{
			return RedirectToAction("Index","Order");
		}

		[HttpGet]
		public IActionResult Member()
		{
			return RedirectToAction("Index", "Members");
		}

		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public IActionResult Create(ProductViewModel Product)
		{
			try
			{
				string data = JsonConvert.SerializeObject(Product);
				StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
				HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/Product/CreateProduct", content).Result;
				if (response.IsSuccessStatusCode)
				{
					TempData["Success"] = "Product Created";
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				TempData["Error"] = "Product Create Failed";
				return View();
			}
			return View();
		}

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {

                Product product = new Product();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Product/GetProductByProductId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    product = JsonConvert.DeserializeObject<Product>(data);
                    product.ProductId = id;
                }
                return View(product);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost]
        public IActionResult Edit(Product product)
        {
            try
            {
                string data = JsonConvert.SerializeObject(product);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PutAsync(_client.BaseAddress + "/Product/UpdateProduct/" + product.ProductId, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Member Update Failed";
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            try
            {
                Product product = new Product();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Product/GetProductByProductId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    product = JsonConvert.DeserializeObject<Product>(data);
                    product.ProductId = id;
                }
                return View(product);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + "/Product/DeleteProduct/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["Success"] = "Product Deleted";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Member Update Failed";
                return View();
            }
            return View();
        }
    }
}
