﻿using AutoMapper;
using AutoMapper.Execution;
using BusinessObject.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using Web.Models;
using BusinessObject.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpClient _client;

        Uri baseAddress = new Uri("https://localhost:7224/api");

        public HomeController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;

        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginModel model)
        {
            try
            {
                var config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
                var email = config.GetSection("AdminAccount:email").Value;
				var password = config.GetSection("AdminAccount:password").Value;
				BusinessObject.Models.Member member = new BusinessObject.Models.Member();
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/Member/Login", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data1 = response.Content.ReadAsStringAsync().Result;
                    member = JsonConvert.DeserializeObject<BusinessObject.Models.Member>(data1);
                    return RedirectToAction("Index","Product");
                }else if(model.Password == password && model.Email == email
                    )
                {
					return RedirectToAction("Index", "Product");
				}
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Login Failed";
                return View();
            }
            return View();
        }


    }
}