﻿using BusinessObject.Models;
using DataAccess.Commons;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;
using Web.Models;

namespace Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly HttpClient _client;

        Uri baseAddress = new Uri("https://localhost:7224/api");

        public OrderController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;

        }

		[HttpGet]
		public IActionResult Product()
		{
			return RedirectToAction("Index", "Product");
		}

		[HttpGet]
		public IActionResult Member()
		{
			return RedirectToAction("Index", "Members");
		}

		[HttpGet]
        public IActionResult Index()
        {
            Pagination<Order> orderList = new Pagination<Order>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Order/GetAllOrders").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                orderList = JsonConvert.DeserializeObject<Pagination<Order>>(data);
            }
            return View(orderList.Items);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Order Order)
        {
            try
            {
                string data = JsonConvert.SerializeObject(Order);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/Order/CreateOrder", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["Success"] = "Order Created";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Order Create Failed";
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {

                Order order = new Order();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Order/GetOrderByOrderId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    order = JsonConvert.DeserializeObject<Order>(data);
                    order.OrderId = id;
                }
                return View(order);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost]
        public IActionResult Edit(Order order)
        {
            try
            {
                string data = JsonConvert.SerializeObject(order);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PutAsync(_client.BaseAddress + "/Order/UpdateOrder/" + order.OrderId, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "Order Update Failed";
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            try
            {
                Order order = new Order();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Order/GetOrderByOrderId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    order = JsonConvert.DeserializeObject<Order>(data);
                    order.OrderId = id;
                }
                return View(order.OrderDetails);
            }
            catch (Exception)
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            try
            {
                Order order = new Order();
                HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Order/GetOrderByOrderId/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    order = JsonConvert.DeserializeObject<Order>(data);
                    order.OrderId = id;
                }
                return View(order);
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + "/Order/Deleteorder/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["Success"] = "order Deleted";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                TempData["Error"] = "order Delet Failed";
                return View();
            }
            return View();
        }
    }
}
