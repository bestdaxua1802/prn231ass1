﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class MemberViewModel
    {
        [Required]
        public int MemberId { get; set; }
        [Required]
        public string Email { get; set; } = null!;
        [Required]
        public string CompanyName { get; set; } = null!;
        [Required]
        public string City { get; set; } = null!;
        [Required]
        public string Country { get; set; } = null!;
        [Required]
        public string Password { get; set; } = null!;
    }
}
