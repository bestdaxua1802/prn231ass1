﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        [Required]
        public int? CategoryId { get; set; }
        [Required]
        public string ProductName { get; set; } = null!;
        [Required]
        public string Weight { get; set; } = null!;
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public int UnitsInStock { get; set; }
    }
}
